import os
import fnmatch
jpgCounter = 0
# В цикле, с помощью os.listdir('.') получим список файлов
# в текущей директории (точка в скобках как раз ее и обозначает)
for fname in os.listdir('.'):
    # Если у текущего имени файла расширение .jpg, то считаем его
    if fnmatch.fnmatch(fname, '*.jpg'):
        #print(fname)
        jpgCounter += 1
print(jpgCounter/2)
